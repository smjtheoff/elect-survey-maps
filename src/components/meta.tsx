import React, { FC } from "react";
import Head from "next/head";

const Meta: FC = () => {
  return (
    <Head>
      <meta name="viewport" content="width=device-width,initial-scale=1" />
      <meta charSet="utf-8" />

      <title>We Check 66</title>
    </Head>
  );
};

export default Meta;
